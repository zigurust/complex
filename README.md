# Readme

A simple example for returning objects of generic types using complex numbers as the main idea.

## Build

* Build with zig 0.10.1
* `zig build run` to see the exampleee output

## License

[MIT](./LICENSE)

