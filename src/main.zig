const std = @import("std");

pub fn Complex(comptime F: type) type {
    return struct {
        const Self = @This();
        re: F,
        im: F,
        pub fn add(self: Self, other: Self) Self {
            //const re = self.re + other.re;
            //const im = self.im + other.im;
            //const ComplexType = Complex(F);
            //const complex = ComplexType{
            //    .re = re,
            //    .im = im,
            //};
            //return complex;
            return Self{
                .re = self.re + other.re,
                .im = self.im + other.im,
            };
        }
    };
}

pub fn newComplex(comptime F: type, re: F, im: F) @TypeOf(Complex(F)) {
    const ComplexF = Complex(F);
    const complex = ComplexF{
        .re = re,
        .im = im,
    };

    return complex;
}

pub fn main() !void {
    std.debug.print("Complex number example\n", .{});
    const ComplexF32 = Complex(f32);
    const c1 = ComplexF32{
        .re = 1.0,
        .im = 0.0,
    };
    const c2 = ComplexF32{
        .re = 0.0,
        .im = 1.0,
    };

    const c3 = c1.add(c2);

    std.debug.print("re = {}, im = {}\n", .{ c3.re, c3.im });

    std.debug.print("Another example\n", .{});

    const complex_1 = newComplex(f64, 1.0, 1.0);
    const complex_2 = newComplex(f64, -1.0, -1.0);
    const complex_3 = complex_1.add(complex_2);

    std.debug.print("re = {}, im = {}\n", .{ complex_3.re, complex_3.im });
}

test "simple test" {
    var list = std.ArrayList(i32).init(std.testing.allocator);
    defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
    try list.append(42);
    try std.testing.expectEqual(@as(i32, 42), list.pop());
}
